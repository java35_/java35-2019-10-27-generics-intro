import java.util.Iterator;

public class MainAppl {
	public static void main(String[] args) {
		MyArray<Integer> array = new MyArray<>();
		
		array.add(2);
		array.add(3);
		array.add(4);
		array.add(5);
		array.add(6);
		
		
		// Iterable
		// Iterator
		
		Iterator<Integer> itr = array.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
}
