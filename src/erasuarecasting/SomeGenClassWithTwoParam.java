package erasuarecasting;

public class SomeGenClassWithTwoParam<T, V> {
	T valueT;
	V valueV;
	
	public SomeGenClassWithTwoParam(T valueT, V valueV) {
		this.valueT = valueT;
		this.valueV = valueV;
	}
	
	public void set(T value) {
		this.valueT = value;
	}
	
	public void set(V value) {
		this.valueV = value;
	}
}
