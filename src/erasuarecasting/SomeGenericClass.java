package erasuarecasting;

public class SomeGenericClass<T> {
	T value;

	public SomeGenericClass(T value) {
		this.value = value;
	}
	
	public T get() {
		return value;
	}
	
	public Object get() {
		return (Object)value;
	}
	
	public void set(T value) {
		
	}
	
	public void set(Object value) {
		
	}
}
