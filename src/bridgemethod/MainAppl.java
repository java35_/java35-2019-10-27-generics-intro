package bridgemethod;
import java.lang.reflect.Method;

public class MainAppl {

	public static void main(String[] args) {
		Child child = new Child();
		
		System.out.println(child.getValue());
		
		Method[] method = Child.class.getMethods();
		
		for (Method method2 : method) {
			System.out.println(method2 + " " + method2.isBridge());
		}
	}

}