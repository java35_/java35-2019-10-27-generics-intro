package bridgemethod;

public class Parent<T> {
	T value;
	
	public T getValue() {
		return value;
	}
}