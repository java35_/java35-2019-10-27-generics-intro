package bridgemethod;

public class Child extends Parent<Integer> {
	Integer value = 10;
	
	@Override
	public Integer getValue() {
		return value;
	};
	
	public Object getValue() {
		return (Object)this.getValue();
	};
}
