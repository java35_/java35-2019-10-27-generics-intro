package wildcards;

public class Person<T extends Number> {
	T salary;

	public Person(T salary) {
		this.salary = salary;
	}
	
	public boolean isSalaryEquals(Person<? extends Number> person) {
		return person.salary.doubleValue() == this.salary.doubleValue();
	}
}