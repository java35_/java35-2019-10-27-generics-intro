
public interface IItr<E> {
	E getNextElement();
	public void resetCounterElement();
	public boolean hasNextElement();
}