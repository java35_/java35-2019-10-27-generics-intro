package arraygenerics;
public class Dog {
	private int i;

	public Dog(int i) {
		this.i = i;
	}
	
	public void gav() {
		System.out.println("Dog " + i +" gav!");
	}
}