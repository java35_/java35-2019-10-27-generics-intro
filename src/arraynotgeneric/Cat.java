package arraynotgeneric;
public class Cat {
	private int i;

	public Cat(int i) {
		this.i = i;
	}
	
	public void miau() {
		System.out.println("Cat " + i +" miau!");
	}
}
