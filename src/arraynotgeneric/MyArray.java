package arraynotgeneric;

public class MyArray {
	private Object array[] = new Object[10];
	private int size = 0;
	
	public boolean add(Object obj) {
		if (size == array.length)
			return false;
		array[size] = obj;
		size++;
		return true;
	}
	
	public Object get(int index) {
		if (index < 0 || index > size - 1)
			return null;
		return array[index];
	}
	
	public int getSize() {
		return size;
	}
}	
