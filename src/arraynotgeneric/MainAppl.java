package arraynotgeneric;
public class MainAppl {

	public static void main(String[] args) {
		MyArray cats = new MyArray();
		
		cats.add(new Cat(1));
		cats.add(new Cat(2));
		cats.add(new Cat(3));
		cats.add(new Cat(4));
		cats.add(new Cat(5));
		cats.add(new Cat(6));
		
//		for (int i = 0; i < cats.getSize(); i++) {
//			Cat tmpCat = (Cat)cats.get(i);
//			tmpCat.miau();
//		}
		
//		for (int i = 0; i < cats.getSize(); i++) {
//			((Cat)cats.get(i)).miau();
//		}
		
		cats.add(new Dog(1));
		
		for (int i = 0; i < cats.getSize(); i++) {
			if (cats.get(i) instanceof Cat) {
				Cat tmpCat = (Cat)cats.get(i);
				tmpCat.miau();
			}
		}
	}

}