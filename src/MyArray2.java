
public class MyArray2<T> {
	private Object array[] = new Object[10];
	private int size = 0;
	
	public boolean add(T obj) {
		if (size == array.length)
			return false;
		array[size] = obj;
		size++;
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public T get(int index) {
		if (index < 0 || index > size - 1)
			return null;
		return (T)array[index];
	}
	
	public int getSize() {
		return size;
	}
	
	public IItr<T> getItr() {
		return new Itr<>();
	}
	
	public class Itr<E> implements IItr<E> {
	
		int counterElement = 0;
		
		@Override
		public E getNextElement() {
			if (size == 0)
				return null;
			return (E)array[counterElement++];
		}
		
		@Override
		public void resetCounterElement() {
			counterElement = 0;
		}
		
		@Override
		public boolean hasNextElement() {
			return counterElement != size;
		}
	}
	
}