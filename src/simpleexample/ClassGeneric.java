package simpleexample;
public class ClassGeneric<T> {
//	T value;
//	
//	public ClassGeneric(T value) {
//		this.value = value;
//	}
//	
//	public T getValue() {
//		return value;
//	}
//	
//	public void setValue(T value) {
//		this.value = value;
//	}
	
	Object value;
	
	public ClassGeneric(T value) {
		this.value = value;
	}
	
	@SuppressWarnings("unchecked")
	public T getValue() {
		return (T)value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
}