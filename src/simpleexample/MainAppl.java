package simpleexample;
public class MainAppl {

	public static void main(String[] args) {
		// Generic class ******************************
		System.out.println("Generic class ***********");
		ClassGeneric<Integer> classGeneric = 
						new ClassGeneric<>(10);
		System.out.println(classGeneric.value);
		System.out.println(classGeneric.getValue());
		classGeneric.setValue(15);
		System.out.println(classGeneric.getValue());
		
		
		
		// Non generic class ******************************
		System.out.println("Non generic class ***********");
		ClassNonGeneric classNonGeneric = 
										new ClassNonGeneric(20);
		System.out.println(classNonGeneric.value);
		System.out.println(classNonGeneric.getValue());
		classNonGeneric.setValue(25);
		System.out.println(classNonGeneric.getValue());
	}

}