package simpleexample;
public class ClassNonGeneric {
//	Integer value;
//	
//	public ClassNonGeneric(Integer value) {
//		this.value = value;
//	}
//	
//	public Integer getValue() {
//		return value;
//	}
//	
//	public void setValue(Integer value) {
//		this.value = value;
//	}
	
	Object value;
	
	public ClassNonGeneric(Integer value) {
		this.value = value;
	}
	
	public Integer getValue() {
		return (Integer)value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
}