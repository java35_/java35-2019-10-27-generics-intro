import java.util.Iterator;

public class MyArray<T> implements Iterable<T> {
	private Object array[] = new Object[10];
	private int size = 0;
	
	public boolean add(T obj) {
		if (size == array.length)
			return false;
		array[size] = obj;
		size++;
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public T get(int index) {
		if (index < 0 || index > size - 1)
			return null;
		return (T)array[index];
	}
	
	public int getSize() {
		return size;
	}

	@Override
	public Iterator<T> iterator() {
		return new Itr<>();
	}
	
	class Itr<E> implements Iterator<E> {

		int counterElement = 0;
		
		@Override
		public boolean hasNext() {
			return counterElement != size;
		}

		@Override
		@SuppressWarnings("unchecked")
		public E next() {
			if (size == 0)
				return null;
			return (E)array[counterElement++];
		}
		
	}
	
}